import { Component, OnInit } from '@angular/core';
import { Http, Headers } from '@angular/http';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'Usuários FTP';
  users: FtpUser[] = [];
  http: Http;

  constructor(http: Http) {
      this.http = http;
  }

  ngOnInit() {
    let headers: Headers = new Headers();
    //headers.append('Access-Control-Allow-Headers', 'Content-Type');
    //headers.append('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    //headers.append('Access-Control-Allow-Origin', '*');
    this.http.get('http://localhost:8080/ftpUsers', {headers: headers})
      .map(r => r.json())
      .subscribe(r => {
        this.users = r._embedded.ftpUsers;
        console.log(this.users);
      });
  }

}

class FtpUser {
  username:string;
  password:string;
  shell:string;
  homeDir:string;
}
