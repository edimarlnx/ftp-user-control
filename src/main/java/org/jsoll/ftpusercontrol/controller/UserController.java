package org.jsoll.ftpusercontrol.controller;

import org.jsoll.ftpusercontrol.model.FtpUser;
import org.jsoll.ftpusercontrol.repository.FtpUserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api")
public class UserController {

    @Autowired
    private FtpUserRepository ftpUserRepository;

    @RequestMapping(value = "/all", method = RequestMethod.GET)
    public List<FtpUser> find() {
        return ftpUserRepository.findAll();
    }
}
