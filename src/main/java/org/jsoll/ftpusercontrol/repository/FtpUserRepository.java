package org.jsoll.ftpusercontrol.repository;

import org.jsoll.ftpusercontrol.model.FtpUser;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.web.bind.annotation.CrossOrigin;

@CrossOrigin("*")
@RepositoryRestResource(collectionResourceRel = "ftpUsers", path = "ftpUsers",
        excerptProjection = FtpUser.class)
public interface FtpUserRepository extends PagingAndSortingRepository<FtpUser, Integer> {
    FtpUser findByUsername(@Param("username") String username);
}
