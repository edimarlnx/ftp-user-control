package org.jsoll.ftpusercontrol.repository;

import org.jsoll.ftpusercontrol.model.FtpGroup;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "ftpGroups", path = "ftpGroups")
public interface FtpGroupRepository extends PagingAndSortingRepository<FtpGroup, Integer> {
}
