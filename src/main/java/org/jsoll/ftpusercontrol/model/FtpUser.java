package org.jsoll.ftpusercontrol.model;

import lombok.AccessLevel;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.data.rest.core.annotation.RestResource;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Entity
@Table(name = "ftp_user")
@Data
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class FtpUser {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ftp_user_id")
    private Integer ftpUserId;

    @NotEmpty(message = "${entity.ftp_user.username.empty}")
    @Column(name = "username", length = 64)
    private String username;

    @NotEmpty(message = "${entity.ftp_user.password.empty}")
    @Column(name = "password", length = 64)
    private String password;

    @NotNull(message = "${entity.ftp_user.grupo.null}")
    @ManyToOne
    @JoinColumn(name = "ftp_group_id")
    @RestResource(path = "ftpGroups", rel = "ftpGroup")
    private FtpGroup ftpGroup;

    @NotEmpty(message = "${entity.ftp_user.home-dir.empty}")
    @Column(name = "home_dir", length = 32)
    private String homeDir;

    @NotEmpty(message = "${entity.ftp_user.shell.empty}")
    @Column(name = "shell")
    private String shell = "/sbin/nologin";

    @Column(name = "count")
    private int count;

    @Column(name = "date_time_accessed"/*, columnDefinition = "timestamp without time zone"*/)
    private LocalDateTime dateTimeAccessed;

    @Column(name = "date_time_modified"/*, columnDefinition = "timestamp without time zone"*/)

    private LocalDateTime dateTimeModified;

    public FtpUser(String username, String password, FtpGroup ftpGroup, String homeDir) {
        this.username = username;
        this.password = password;
        this.ftpGroup = ftpGroup;
        this.homeDir = homeDir;
    }
}
