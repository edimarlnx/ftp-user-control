package org.jsoll.ftpusercontrol.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.AccessLevel;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.*;
import java.util.Set;
import java.util.TreeSet;

@Entity
@Table(name = "ftp_group")
@Data
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class FtpGroup {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ftp_group_id")
    private Integer ftpGroupId;

    @NotEmpty(message = "${entity.ftp_group.name.empty}")
    @Column(name = "name", length = 64)
    private String name;

    @JsonBackReference
    @OneToMany(mappedBy = "ftpGroup")
    private Set<FtpUser> ftpUsers = new TreeSet<>();

    public FtpGroup(String name) {
        this.name = name;
    }
}
