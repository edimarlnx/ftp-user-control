package org.jsoll.ftpusercontrol;

import org.jsoll.ftpusercontrol.FtpUserControlApplication;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {FtpUserControlApplication.class})
public class FtpFtpUserControlApplicationTests {
    @Test
    public void contextLoads() throws Exception {
    }
}
